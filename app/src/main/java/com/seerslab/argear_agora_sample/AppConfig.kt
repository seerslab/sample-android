package com.seerslab.argear_agora_sample

class AppConfig {

    companion object {
        @JvmField
        val AGORA_APP_ID = "f790e315bc9c4f84902126784d327539"
        @JvmField
        val AGORA_TOKEN_URL = "http://192.168.1.98:8080"
    }
}