package com.seerslab.argear_agora_sample

import android.os.Bundle
import android.widget.FrameLayout
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.seerslab.argear_agora_sample.api.token.TokenRepository
import com.seerslab.argear_agora_sample.api.token.TokenResponse
import io.agora.rtc.Constants
import io.agora.rtc.IRtcEngineEventHandler
import io.agora.rtc.RtcEngine
import io.agora.rtc.video.VideoCanvas
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class AudienceActivity : AppCompatActivity() {

    private var mRtcEngine: RtcEngine ?= null

    private val mRtcEventHandler = object : IRtcEngineEventHandler() {
        override fun onUserJoined(uid: Int, elapsed: Int) {
            runOnUiThread {
                setupRemoteVideo(uid)
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_audience)

        initializeAndJoinChannel("demo")
    }

    override fun onDestroy() {
        super.onDestroy()

        mRtcEngine?.leaveChannel()
        RtcEngine.destroy()
    }

    private fun initializeAndJoinChannel(channelName: String) {
        try {
            mRtcEngine = RtcEngine.create(baseContext, AppConfig.AGORA_APP_ID, mRtcEventHandler)
        } catch (e: Exception) {

        }
        mRtcEngine!!.enableVideo()
        mRtcEngine!!.enableAudio()

        TokenRepository.instance.loadToken(channelName, object : Callback<TokenResponse> {
            override fun onResponse(call: Call<TokenResponse>, response: Response<TokenResponse>) {
                if (response.isSuccessful) {
                    response.body()?.let {
                        joinChannel(it.token, channelName)
                    }
                }
            }

            override fun onFailure(call: Call<TokenResponse>, t: Throwable) {
                Toast.makeText(this@AudienceActivity, "Failed to get token", Toast.LENGTH_SHORT).show()
                t.printStackTrace()
            }
        })
    }

    private fun joinChannel(token: String, channelName: String) {
        // Join the channel with a token.
        mRtcEngine!!.setChannelProfile(Constants.CHANNEL_PROFILE_LIVE_BROADCASTING)
        mRtcEngine!!.setClientRole(Constants.CLIENT_ROLE_AUDIENCE)
        mRtcEngine!!.joinChannel(token, channelName, "", 0)
    }

    private fun setupRemoteVideo(uid: Int) {
        val remoteContainer = findViewById(R.id.remote_video_view_container) as FrameLayout

        val remoteFrame = RtcEngine.CreateRendererView(baseContext)
        remoteFrame.setZOrderMediaOverlay(true)
        remoteContainer.addView(remoteFrame)
        mRtcEngine!!.setupRemoteVideo(VideoCanvas(remoteFrame, VideoCanvas.RENDER_MODE_FIT, uid))
    }

}