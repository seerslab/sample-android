package com.seerslab.argear_agora_sample.api.token

import com.seerslab.argear_agora_sample.AppConfig
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class TokenService {

    companion object {

        @JvmStatic
        fun createTokenService(): TokenApi {

            val loggingInterceptor = HttpLoggingInterceptor()
            loggingInterceptor.level = HttpLoggingInterceptor.Level.BASIC

            val httpClient = OkHttpClient.Builder().run {
                addInterceptor(loggingInterceptor)
                build()
            }

            return Retrofit.Builder().run {
                baseUrl(AppConfig.AGORA_TOKEN_URL)
                client(httpClient)
                addConverterFactory(GsonConverterFactory.create())
                build()
            }.create(TokenApi::class.java)
        }
    }
}