package com.seerslab.argear_agora_sample.api.token

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface TokenApi {

    @GET("/access_token")
    fun getToken(@Query("channelName") channelName: String): Call<TokenResponse>
}