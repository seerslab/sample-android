package com.seerslab.argear_agora_sample.api.token

import retrofit2.Callback

class TokenRepository {

    companion object {
        @JvmStatic
        val instance: TokenRepository by lazy { TokenRepository() }
    }

    private val tokenApi: TokenApi = TokenService.createTokenService()

    fun loadToken(channelName: String, callback: Callback<TokenResponse>) {
        tokenApi.getToken(channelName).enqueue(callback)
    }
}