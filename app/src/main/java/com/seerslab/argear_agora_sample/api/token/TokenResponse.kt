package com.seerslab.argear_agora_sample.api.token

data class TokenResponse(
    val token: String
)
