package com.seerslab.argear_agora_sample

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.Toast
import com.seerslab.argear.sample.ui.CameraActivity
import com.seerslab.argear_agora_sample.api.token.TokenRepository
import com.seerslab.argear_agora_sample.api.token.TokenResponse
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val btnJoin: Button = findViewById(R.id.btn_join)
        val btnLive: Button = findViewById(R.id.btn_live)

        btnJoin.setOnClickListener {
            val intent = Intent(this@MainActivity, AudienceActivity::class.java)
            startActivity(intent)
        }

        btnLive.setOnClickListener {
            val channelName = "demo"

            TokenRepository.instance.loadToken(channelName, object : Callback<TokenResponse> {
                override fun onResponse(call: Call<TokenResponse>, response: Response<TokenResponse>) {
                    if (response.isSuccessful) {
                        response.body()?.let {
                            val intent = Intent(this@MainActivity, CameraActivity::class.java)
                            intent.putExtra("agoraAppId", AppConfig.AGORA_APP_ID)
                            intent.putExtra("token", it.token)
                            intent.putExtra("channelName", channelName)
                            startActivity(intent)
                        }
                    }
                }

                override fun onFailure(call: Call<TokenResponse>, t: Throwable) {
                    Toast.makeText(this@MainActivity, "Failed to get token", Toast.LENGTH_SHORT).show()
                    t.printStackTrace()
                }
            })
        }
    }
}